![](img/rust-logo.png)
<!-- .element: style="margin-top: -5%;" -->
## Introducción a Rust

#### [Mario Garcia](https://mariog.xyz) · [@mariogmd](https://twitter.com/mariogmd)

---

## ¿Qué es Rust?

----

### Un lenguaje de programación de sistemas enfocado en:

<br>

<span class="fragment">seguridad</span><span class="fragment">, velocidad</span><span class="fragment">, y concurrencia.</span>

<!-- .element: class="fragment" -->

---

## Instalando Rust

----

### Canales de lanzamiento

- Stable
- Beta<!-- .element: class="fragment" -->
- Nightly<!-- .element: class="fragment" -->

----

### rustup

https://rustup.rs/

----

#### Compilador

```
  $ rustc --version
  rustc 1.44.1 (c7087fe00 2020-06-17)
```

----

#### Cargo

- Gestor de paquetes
- Herramienta de construcción
- Ejecutor de pruebas
- Generador de documentación

---

## rustup

----

### Instalación

```
  $ rustup install nightly
```

----

### Actualización

```
  $ rustup update
```

----

### Definir toolchain por defecto

```
  $ rustup default nightly
```

----

### Definir toolchain para proyecto

```
rustup override set nightly
```

---

## Configurando un Proyecto

```
  $ cargo new hola_mundo
  $ cd hola_mundo
```

----

### Archivos creados

- Cargo.toml: Metadatos sobre tu proyecto y sus dependencias
- .gitignore: Ignora los archivos compilados construidos por Rust al agregarlos a Git
- src/main.rs: Donde irá tu código Rus

----

## Cargo.toml

```
[package]
name = "hola_mundo"
version = "0.1.0"
authors = ["Tu nombre <nombre@correo.com>"]

[dependencies]
```

----

## main.rs

```
fn main() {
    println!("Hello, world!");
}
```

----

## Ejecutarlo

- cargo run
- Imprime "¡Hola mundo!"
- target: Archivos generados en la compilación
- Cargo.lock: Bloquea tus dependencias

---

## Sintaxis

---

## Imprimir en pantalla

```
  println!("string {} literal", expression);
```

---

## Commentarios

```
  // Este es un comentario
```

---

## Variables

```
let name = "Ashley";
let age = 30;
println!("Hola, {}! Tienes {} años de edad.", name, age);
```

----

## Experimento

```
let apples = 100;
apples += 50;
println!("Tengo {} manzanas", apples);
```

---

## Mutability

----

```
let mut apples = 100;
apples += 50;
println!("Tengo {} manzanas", apples);
```

---

## Tipos

Rust es de tipado fuerte y estático. Los tipos fundamentales son:

- u32: Entero de 32-bit sin signo
- i32: Entero de 32-bit con signo
- f64: Número de coma flotante
- String y/o &str:
- bool: Un boolean

----

## Inferencia de tipos

```
  let x = 42; // x es de tipo i32
  let y = 1.0; // y es de tipo f64
```

----

```
  let x: i32 = 42;
  let y: f64 = 1.0;
```

---

## Condicionales: if

```
  if condition {
      expressions;
  }
  else if condition {
      expressions;
  }
  else {
      expressions;
  }
```

---

## Condicionales: match

```
fn main() {
    let height: u32 = 167;
    match height {
        0..=149 => println!("Eres demasiado pequeño para ir en la montaña rusa."),
        150..=200 => println!("¡Puedes ir en la montaña rusa!"),
        _ => println!("Eres demasiado alto para ir en la montaña rusa."),
    }
}
```

---

## Tuplas

```
  let tup: (i32, f64, u8) = (500, 6.4, 1);
```

---

## Arrays

```
  let a = [1, 2, 3]; // a: [i32; 3]
```

---

## Vectores

```
let mut prices = vec![30, 100, 2];
prices[0] = 25;
prices.push(40);
println!("Todos los precios son: {:?}", prices);
```

---

## Loops

----

### loop

```
  loop {
      expressions;
  }
```

----

### for

```
  for i in x..n {
      expressions;
  }
```

----

```
  fn main () {
      for i in 0..10 {
          println!("Número {}", i);
      }
  }
```

----

```
  fn main() {
      let names = vec!["Carol", "Jake", "Marylou", "Bruce"];
      for name in names.iter() {
          println!("¡Hola {}!", name);
      }
  }
```

----

```
  fn main() {
      let a = [1, 2, 3, 4, 5];
      for n in a.rev() {
          println!("{}", n);
      }
  }
```

----

### while

```
  while condition {
      expressions;
  }
```

----

```
  fn main () {
      let a = [1, 2, 3, 4, 5];
      let mut i = 0;
      while i < 5 {
          println!("{}", a[i]);
          i = i +1;
      }
  }
```

---

## Funciones

```
  fn name(arg: Type) -> ReturnType {
      statements;
  }
```

---

## Example

```
  fn main () {
      let a = 9;
      let b = 15;
      suma(a, b);
  }
  fn suma(a: i32, b: i32) {
      let c = a + b;
      println("La suma de {} + {} es: {}", a, b, c;
  }
```

----

```
  fn five() -> i32 {
      5
  }

  fn main() {
      let x = five();
      println!("The value of x is: {}", x);
  }
```

---

## Learn More

_[rust-lang.org](https//rust-lang.org)_
_[Libro de Rust](https//mattdark.gitlab.io/rust-book)_
___

[@mariogmd](https://twitter.com/mariogmd)

hi@mariog.xyz
